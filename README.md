**РАЗРАБОТКА КОНСОЛЬНОГО ПРИЛОЖЕНИЯ**
=====================

GITLAB URL
-----------------------------------

[https://gitlab.com/dgordg85/jse-05](https://gitlab.com/dgordg85/jse-05 "GITLAB")

ТРЕБОВАНИЯ К SOFTWARE
-----------------------------------
* JRE

ОПИСАНИЕ СТЕКА ТЕХНОЛОГИЙ
-----------------------------------
* JDK 1.8
* MAVEN 3.4.0
* IDEA

ИМЯ РАЗРАБОТЧИКА И КОНТАКТЫ
-----------------------------------
    Александр К.
    Skype: roverc0m
    E-mail: dgordg85@gmail.com

КОМАНДЫ ДЛЯ СБОРКИ ПРИЛОЖЕНИЯ
-----------------------------------
    git clone http://gitlab.volnenko.school/Kozyrev/jse-05.git
    cd jse-05
    mvn clean install
    
КОМАНДЫ ДЛЯ ЗАПУСКА ПРИЛОЖЕНИЯ
-----------------------------------
    java -jar ./task-manager-1.05.jar
