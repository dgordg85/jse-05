package ru.kozyrev.tm.util;

import ru.kozyrev.tm.exception.IndexException;

public class StringUtil {

    public static int parseToInt(String num) throws IndexException {
        try {
            return Integer.parseInt(num);
        } catch (NumberFormatException e) {
            throw new IndexException();
        }
    }
}
