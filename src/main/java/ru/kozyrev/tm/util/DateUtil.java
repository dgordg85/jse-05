package ru.kozyrev.tm.util;

import ru.kozyrev.tm.exception.DateException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    private static SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

    public static String getDate(Date date) {
        return formatter.format(date);
    }

    public static Date parseDate(String date) throws Exception {
        try {
            if (date == null || date.isEmpty()) {
                throw new DateException();
            }
            return formatter.parse(date);
        } catch (DateException | ParseException e) {
            throw e;
        }
    }
}
