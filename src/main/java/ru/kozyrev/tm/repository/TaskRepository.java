package ru.kozyrev.tm.repository;

import ru.kozyrev.tm.entity.Task;

import java.util.*;

public class TaskRepository extends AbstractRepository<Task> {
    private Map<String, Task> map = new LinkedHashMap<>();

    public List<Task> findAll() {
        return new ArrayList<>(map.values());
    }

    public Task findOne(String id) {
        return map.get(id);
    }

    public Task persist(Task task) {
        map.put(task.getId(), task);
        return task;
    }

    public Task merge(Task task) {
        map.put(task.getId(), task);
        return task;
    }

    public Task remove(String id) {
        return map.remove(id);
    }

    public void removeAll() {
        map.clear();
    }
}
