package ru.kozyrev.tm.repository;

import ru.kozyrev.tm.entity.Project;

import java.util.*;

public class ProjectRepository extends AbstractRepository<Project> {
    private Map<String, Project> map = new LinkedHashMap<>();

    public List<Project> findAll() {
        return new ArrayList<>(map.values());
    }

    public Project findOne(String id) {
        return map.get(id);
    }

    public Project persist(Project project) {
        map.put(project.getId(), project);
        return project;
    }

    public Project merge(Project project) {
        map.put(project.getId(), project);
        return project;
    }

    public Project remove(String id) {
        return map.remove(id);
    }

    public void removeAll() {
        map.clear();
    }
}
