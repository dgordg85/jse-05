package ru.kozyrev.tm.repository;

import java.util.List;

public abstract class AbstractRepository<T> {
    public abstract List<T> findAll();
    public abstract T findOne(String id);
    public abstract T persist(T abstractEntity);
    public abstract T merge(T abstractEntity);
    public abstract T remove(String id);
    public abstract void removeAll();
}
