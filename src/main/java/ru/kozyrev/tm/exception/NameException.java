package ru.kozyrev.tm.exception;

public class NameException extends Exception {
    public NameException() {
        super("Wrong name!");
    }
}
