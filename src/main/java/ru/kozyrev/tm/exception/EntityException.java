package ru.kozyrev.tm.exception;

public class EntityException extends Exception {
    public EntityException() {
        super("Entity work error!");
    }
}
