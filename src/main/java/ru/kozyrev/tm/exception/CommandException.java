package ru.kozyrev.tm.exception;

public class CommandException extends Exception {
    public CommandException() {
        super("Wrong command! Use help!");
    }
}
