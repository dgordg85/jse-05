package ru.kozyrev.tm.exception;

public class IndexException extends Exception {
    public IndexException() {
        super("Wrong index!");
    }
}
