package ru.kozyrev.tm.exception;

public class DescriptionException extends Exception {
    public DescriptionException() {
        super("Wrong Description");
    }
}
