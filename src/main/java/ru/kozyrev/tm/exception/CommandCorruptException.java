package ru.kozyrev.tm.exception;

public class CommandCorruptException extends Exception {
    public CommandCorruptException() {
        super("Command name or description is broken");
    }
}
