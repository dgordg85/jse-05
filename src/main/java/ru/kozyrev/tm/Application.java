package ru.kozyrev.tm;


import ru.kozyrev.tm.context.Bootstrap;

public class Application {
    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
