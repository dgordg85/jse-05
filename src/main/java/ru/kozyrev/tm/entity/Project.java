package ru.kozyrev.tm.entity;

import ru.kozyrev.tm.util.DateUtil;

import java.util.Date;
import java.util.UUID;

public class Project {
    private String id = UUID.randomUUID().toString();
    private String name = "";
    private String description = "";
    private Date dateStart;
    private Date dateFinish;

    public Project() {
    }

    public Project(String name, String description, String dateStart, String dateFinish) throws Exception {
        this.name = name;
        this.description = description;
        this.dateStart = DateUtil.parseDate(dateStart);
        this.dateFinish = DateUtil.parseDate(dateFinish);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public String getDateStartAsStr() {
        return DateUtil.getDate(dateStart);
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public String getDateFinishAsStr() {
        return DateUtil.getDate(dateFinish);
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }
}
