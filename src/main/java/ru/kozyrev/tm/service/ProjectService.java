package ru.kozyrev.tm.service;

import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.exception.DateException;
import ru.kozyrev.tm.exception.DescriptionException;
import ru.kozyrev.tm.exception.IndexException;
import ru.kozyrev.tm.exception.NameException;
import ru.kozyrev.tm.repository.ProjectRepository;
import ru.kozyrev.tm.util.StringUtil;

import java.util.List;

public class ProjectService {
    ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project findOne(String id) {
        if (id == null || id.length() == 0)
            return null;
        return projectRepository.findOne(id);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project persist(Project project) throws Exception {
        if (project.getName() == null || project.getName().isEmpty()){
            throw new NameException();
        }
        if (project.getDescription() == null || project.getDescription().isEmpty()){
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }
        return projectRepository.persist(project);
    }

    public Project merge(Project project) throws Exception {
        if (project.getName() == null){
            throw new NameException();
        }
        if (project.getDescription() == null){
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }
        Project projectUpdate = findOne(project.getId());
        if (project.getName().length() != 0) {
            projectUpdate.setName(project.getName());
        }
        if (project.getDescription().length() != 0) {
            projectUpdate.setDescription(project.getDescription());
        }
        projectUpdate.setDateStart(project.getDateStart());
        projectUpdate.setDateFinish(project.getDateFinish());
        return projectRepository.merge(projectUpdate);
    }

    public Project remove(String num) throws IndexException {
        if (num == null || num.isEmpty())
            return null;
        return projectRepository.remove(getProjectIdByNum(num));
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

    public Project getProjectByNum(String num) throws IndexException {
        if (num == null || num.length() == 0) {
            throw new IndexException();
        }

        int index = StringUtil.parseToInt(num) - 1;
        List<Project> list = findAll();

        if (index < 0 || index > list.size() - 1) {
            throw new IndexException();
        }
        return list.get(index);
    }

    public String getProjectIdByNum(String num) throws IndexException {
        return getProjectByNum(num).getId();
    }
}
