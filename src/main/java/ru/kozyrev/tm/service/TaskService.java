package ru.kozyrev.tm.service;

import ru.kozyrev.tm.context.Bootstrap;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.DateException;
import ru.kozyrev.tm.exception.DescriptionException;
import ru.kozyrev.tm.exception.IndexException;
import ru.kozyrev.tm.exception.NameException;
import ru.kozyrev.tm.repository.ProjectRepository;
import ru.kozyrev.tm.repository.TaskRepository;
import ru.kozyrev.tm.util.StringUtil;

import java.util.Iterator;
import java.util.List;

public class TaskService {
    TaskRepository taskRepository;
    TaskService taskService;
    ProjectRepository projectRepository;
    ProjectService projectService;

    public TaskService(Bootstrap bootstrap) {
        this.taskRepository = bootstrap.getTaskRepository();
        //this.projectRepository = bootstrap.getProjectRepository();
        this.taskService = bootstrap.getTaskService();
        this.projectService = bootstrap.getProjectService();
    }

    public Task findOne(String id) {
        if (id == null || id.isEmpty())
            return null;
        return taskRepository.findOne(id);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task persist(Task task) throws Exception {
        if (task.getName() == null || task.getName().isEmpty()) {
            throw new NameException();
        }
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
            throw new IndexException();
        }
        if (task.getDescription() == null || task.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (task.getDateStart() == null) {
            throw new DateException();
        }
        if (task.getDateFinish() == null) {
            throw new DateException();
        }
        return taskRepository.persist(task);
    }

    public Task merge(Task task) throws Exception {
        if (task.getName() == null) {
            throw new NameException();
        }
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
            throw new IndexException();
        }
        if (task.getDescription() == null) {
            throw new DescriptionException();
        }
        if (task.getDateStart() == null) {
            throw new DateException();
        }
        if (task.getDateFinish() == null) {
            throw new DateException();
        }

        Task taskUpdate = findOne(task.getId());

        if (task.getName().length() != 0) {
            taskUpdate.setName(task.getName());
        }
        taskUpdate.setProjectId(task.getProjectId());

        if (task.getDescription().length() != 0)
            taskUpdate.setDescription(task.getDescription());

        taskUpdate.setDateStart(task.getDateStart());
        taskUpdate.setDateFinish(task.getDateFinish());
        return taskRepository.merge(taskUpdate);
    }

    public Task remove(String taskNum) throws IndexException {
        if (taskNum == null || taskNum.isEmpty()) {
            return null;
        }
        return taskRepository.remove(getTaskIdByNum(taskNum));
    }

    public boolean removeProjectTasks(String projectNum) throws IndexException {
        boolean isDelete = false;
        String projectId = projectService.getProjectIdByNum(projectNum);
        List<Task> list = findAll();
        Iterator<Task> iterator = list.iterator();
        Task task;
        while (iterator.hasNext()) {
            task = iterator.next();
            if (task.getProjectId().equals(projectId)) {
                taskRepository.remove(task.getId());
                isDelete = true;
            }
        }
        return isDelete;
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public Task getTaskByNum(String num) throws IndexException {
        if (num == null || num.isEmpty()) {
            throw new IndexException();
        }

        int index = StringUtil.parseToInt(num) - 1;
        List<Task> list = findAll();

        if (index < 0 || index > list.size() - 1) {
            throw new IndexException();
        }
        return list.get(index);
    }

    public String getTaskIdByNum(String num) throws IndexException {
        return getTaskByNum(num).getId();
    }
}
