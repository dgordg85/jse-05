package ru.kozyrev.tm.command;

import ru.kozyrev.tm.exception.IndexException;

public class TaskClearCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws IndexException {
        System.out.println("[CLEAR]");
        System.out.println("Press 'ENTER' FOR ALL OR PROJECT ID]");
        String projectNum = bootstrap.getSc().nextLine();
        if (projectNum.length() == 0) {
            bootstrap.clearAllTasks();
        } else {
            bootstrap.clearTasks(projectNum);
        }
    }
}
