package ru.kozyrev.tm.command;

public class HelpCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS LIST]");
        bootstrap.printCommands();
    }
}
