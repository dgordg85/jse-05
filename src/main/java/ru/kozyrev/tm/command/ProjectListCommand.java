package ru.kozyrev.tm.command;

public class ProjectListCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Press 'ENTER' for printing all projects list or input 'ID PROJECT'");
        String projectNum = bootstrap.getSc().nextLine();
        if (projectNum.length() == 0) {
            bootstrap.printProjects();
        } else {
            bootstrap.printTasks(projectNum);
        }
    }
}
