package ru.kozyrev.tm.command;

import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.EntityException;
import ru.kozyrev.tm.util.DateUtil;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new tasks.";
    }

    @Override
    public void execute() throws Exception {
        Task task = new Task();
        System.out.println("[TASK CREATE]\nENTER NAME:");
        task.setName(bootstrap.getSc().nextLine());
        System.out.println("ENTER PROJECT ID:");
        task.setProjectId(bootstrap.getProjectService().getProjectIdByNum(bootstrap.getSc().nextLine()));
        System.out.println("ENTER DESCRIPTION:");
        task.setDescription(bootstrap.getSc().nextLine());
        System.out.println("ENTER DATESTART:");
        task.setDateStart(DateUtil.parseDate(bootstrap.getSc().nextLine()));
        System.out.println("ENTER DATEFINISH:");
        task.setDateFinish(DateUtil.parseDate(bootstrap.getSc().nextLine()));
        if (bootstrap.getTaskService().persist(task) == null) {
            throw new EntityException();
        }
        System.out.println("[OK]");
    }
}
