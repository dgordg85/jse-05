package ru.kozyrev.tm.command;

public class ExitCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Quit from manager.";
    }

    @Override
    public void execute() {
        bootstrap.getSc().close();
        System.exit(0);
    }
}
