package ru.kozyrev.tm.command;

import ru.kozyrev.tm.context.Bootstrap;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;

    public void init(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();
    public abstract String getDescription();
    public abstract void execute() throws Exception;
}
