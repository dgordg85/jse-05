package ru.kozyrev.tm.command;

import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.util.DateUtil;

import java.text.ParseException;

public class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        Project project = new Project();
        System.out.println("[PROJECT CREATE]\nENTER NAME:");
        project.setName(bootstrap.getSc().nextLine());
        System.out.println("ENTER DESCRIPTION:");
        project.setDescription(bootstrap.getSc().nextLine());
        System.out.println("ENTER DATESTART:");
        project.setDateStart(DateUtil.parseDate(bootstrap.getSc().nextLine()));
        System.out.println("ENTER DATEFINISH:");
        project.setDateFinish(DateUtil.parseDate(bootstrap.getSc().nextLine()));
        bootstrap.getProjectService().persist(project);
        bootstrap.printProjects();
    }
}
