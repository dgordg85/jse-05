package ru.kozyrev.tm.command;

import ru.kozyrev.tm.exception.IndexException;

public class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws IndexException {
        System.out.println("[TASK DELETE]\nENTER ID:");
        String taskNum = bootstrap.getSc().nextLine();
        if (bootstrap.getTaskService().remove(taskNum) == null) {
            throw new IndexException();
        }
        System.out.println("[OK]");
    }
}
