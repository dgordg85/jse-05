package ru.kozyrev.tm.command;

import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.EntityException;
import ru.kozyrev.tm.util.DateUtil;

import java.text.ParseException;

public class TaskUpdateCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-update";
    }

    @Override
    public String getDescription() {
        return "Update selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK]\nENTER ID:");
        Task task = new Task();
        task.setId(bootstrap.getTaskService().getTaskIdByNum(bootstrap.getSc().nextLine()));
        System.out.println("NEW NAME:");
        task.setName(bootstrap.getSc().nextLine());
        System.out.println("NEW PROJECT ID:");
        task.setProjectId(bootstrap.getProjectService().getProjectIdByNum(bootstrap.getSc().nextLine()));
        System.out.println("ENTER DESCRIPTION:");
        task.setDescription(bootstrap.getSc().nextLine());
        System.out.println("ENTER DATESTART:");
        task.setDateStart(DateUtil.parseDate(bootstrap.getSc().nextLine()));
        System.out.println("ENTER DATEFINISH:");
        task.setDateFinish(DateUtil.parseDate(bootstrap.getSc().nextLine()));
        if (bootstrap.getTaskService().merge(task) == null) {
            throw new EntityException();
        }
        System.out.println("[OK]");
    }
}
