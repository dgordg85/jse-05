package ru.kozyrev.tm.command;

import ru.kozyrev.tm.exception.IndexException;

public class TaskListCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws IndexException {
        System.out.println("PRESS 'ENTER' FOR ALL OR INPUT ID PROJECT");
        String projectNum = bootstrap.getSc().nextLine();
        if (projectNum.length() == 0) {
            bootstrap.printTasks();
        } else {
            bootstrap.printTasks(projectNum);
        }
    }
}
