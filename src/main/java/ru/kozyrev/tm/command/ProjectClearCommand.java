package ru.kozyrev.tm.command;

public class ProjectClearCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR]");
        System.out.println("Press 'ENTER' FOR ALL OR PROJECT ID]");
        String projectNum = bootstrap.getSc().nextLine();
        if (projectNum.length() == 0) {
            clearAll();
        } else {
            bootstrap.clearProject(projectNum);
        }
    }

    public void clearAll() {
        bootstrap.clearAllTasks();
        bootstrap.getProjectService().removeAll();
        System.out.println("[ALL PROJECTS REMOVED]");
    }
}
