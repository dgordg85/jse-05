package ru.kozyrev.tm.context;

import ru.kozyrev.tm.command.*;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.*;
import ru.kozyrev.tm.repository.ProjectRepository;
import ru.kozyrev.tm.repository.TaskRepository;
import ru.kozyrev.tm.service.ProjectService;
import ru.kozyrev.tm.service.TaskService;

import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Bootstrap {
    private ProjectRepository projectRepository;
    private ProjectService projectService;
    private TaskRepository taskRepository;
    private TaskService taskService;
    private Scanner sc = new Scanner(System.in);
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public Bootstrap() {
        projectRepository = new ProjectRepository();
        taskRepository = new TaskRepository();
        projectService = new ProjectService(projectRepository);
        taskService = new TaskService(this);
    }

    public void init() throws Exception {
        initCommands();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command;
        while (true) {
            try {
                command = sc.nextLine().toLowerCase().replace('_', '-');
                execute(command);
            } catch (IndexException e) {
                System.out.println("Wrong index!");
            } catch (ParseException | DateException e) {
                System.out.println("Wrong date! User dd-MM-YYYY format");
            } catch (CommandException e) {
                System.out.println("Wrong command! User 'help' command!");
            } catch (NameException e) {
                System.out.println("Wrong name!");
            } catch (DescriptionException e) {
                System.out.println("Wrong description!");
            }
        }
    }

    public void execute(String command) throws Exception {
        if (command == null || command.isEmpty()) {
            throw new CommandException();
        }
        AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            throw new CommandException();
        }
        abstractCommand.execute();
    }

    public void initCommands() throws CommandCorruptException {
        registryCommand(new ProjectListCommand());
        registryCommand(new ProjectCreateCommand());
        registryCommand(new ProjectUpdateCommand());
        registryCommand(new ProjectRemoveCommand());
        registryCommand(new ProjectClearCommand());
        registryCommand(new TaskListCommand());
        registryCommand(new TaskClearCommand());
        registryCommand(new TaskCreateCommand());
        registryCommand(new TaskUpdateCommand());
        registryCommand(new TaskRemoveCommand());
        registryCommand(new HelpCommand());
        registryCommand(new ExitCommand());
    }

    public void printTasks() {
        System.out.println("[TASKS LIST]");
        List<Task> taskList = getTaskService().findAll();
        for (int i = 0; i < taskList.size(); i++) {
            Task task = taskList.get(i);
            System.out.printf("%d. %s, %s, s:%s, f:%s\n",
                    i + 1,
                    task.getName(),
                    task.getDescription(),
                    task.getDateStartAsStr(),
                    task.getDateFinishAsStr()
            );
        }
    }

    public void printTasks(String projectNum) throws IndexException {
        String projectId = getProjectService().getProjectIdByNum(projectNum);
        if (projectId == null)
            throw new IndexException();
        System.out.println("[TASKS LIST OF PROJECT]");
        List<Task> taskList = getTaskService().findAll();
        int count = 1;
        for (int i = 0; i < taskList.size(); i++) {
            if (taskList.get(i).getProjectId().equals(projectId)) {
                Task task = taskList.get(i);
                System.out.printf("%d. %s, EDIT ID#%d, %s, s:%s, f:%s\n",
                        count++,
                        task.getName(),
                        i,
                        task.getDescription(),
                        task.getDateStartAsStr(),
                        task.getDateFinishAsStr()
                );
            }
        }
    }

    public void printProjects() {
        System.out.println("[PROJECTS LIST]");
        List<Project> projects = getProjectService().findAll();
        for (int i = 0; i < projects.size(); i++) {
            Project project = projects.get(i);
            System.out.printf("%d. %s, %s, s:%s, f:%s\n",
                    i + 1,
                    project.getName(),
                    project.getDescription(),
                    project.getDateStartAsStr(),
                    project.getDateFinishAsStr()
            );
        }
    }

    public void clearProject(String projectNum) throws IndexException {
        clearTasks(projectNum);
        if (projectService.remove(projectNum) == null) {
            throw new IndexException();
        }
        System.out.println("[PROJECT REMOVED]");
        printProjects();
    }

    public void clearTasks(String projectNum) throws IndexException {
        if (getTaskService().removeProjectTasks(projectNum)) {
            System.out.println("[ALL TASKS OF PROJECT REMOVED]");
        } else {
            System.out.println("[PROJECT HAVE NO TASKS]");
        }
    }

    public void clearAllTasks() {
        getTaskService().removeAll();
        System.out.println("[ALL TASKS REMOVED]");
    }

    private void registryCommand(AbstractCommand command) throws CommandCorruptException {
        if (command.getName() == null || command.getName().isEmpty())
            throw new CommandCorruptException();
        if (command.getDescription() == null || command.getDescription().isEmpty())
            throw new CommandCorruptException();
        command.init(this);
        commands.put(command.getName(), command);
    }

    public void printCommands() {
        for (AbstractCommand command : commands.values()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }

    public Scanner getSc() {
        return sc;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public void setSc(Scanner sc) {
        this.sc = sc;
    }
}
